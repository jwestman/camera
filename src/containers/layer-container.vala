/* layer-container.vala
 *
 * Copyright 2020 James Westman <james@flyingpimonster.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


/**
 * A container that displays all of its children in the same space, on top
 * of each other, with the first child at the bottom.
 */
public class Camera.LayerContainer : Gtk.Container, Gtk.Buildable {
    private List<Gtk.Widget> widgets;


    construct {
        set_has_window(false);
    }


    protected override void get_preferred_width(out int min, out int nat) {
        min = 0;
        nat = 0;
        foreach (var widget in widgets) {
            int this_min, this_nat;
            widget.get_preferred_width(out this_min, out this_nat);
            min = int.max(min, this_min);
            nat = int.max(nat, this_nat);
        }
    }

    protected override void get_preferred_height(out int min, out int nat) {
        min = 0;
        nat = 0;
        foreach (var widget in widgets) {
            int this_min, this_nat;
            widget.get_preferred_height(out this_min, out this_nat);
            min = int.max(min, this_min);
            nat = int.max(nat, this_nat);
        }
    }


    public override void size_allocate(Gtk.Allocation alloc) {
        foreach (var widget in widgets) {
            widget.size_allocate(alloc);
        }

        base.size_allocate(alloc);
    }


    protected override void forall_internal(bool include_internals, Gtk.Callback callback) {
        foreach_widget(widgets, callback);
    }


    protected override void add(Gtk.Widget widget)
            requires (widgets.find(widget) == null) {

        widget.set_parent(this);
        widgets.append(widget);
    }


    protected override void remove(Gtk.Widget widget)
            requires (widgets.find(widget) != null) {

        widget.unparent();
        widgets.remove(widget);
    }
}
