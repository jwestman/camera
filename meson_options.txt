option('profile', type: 'combo', choices: ['default', 'development'], value: 'default', description: 'Use the Development profile to create an app with Nightly branding')

option('executable_name', type: 'string', value: 'camera', description: 'Name of the final executable')
